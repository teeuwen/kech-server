\documentclass[11pt,a4paper]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[dutch]{babel}

\usepackage[margin=1.0in]{geometry}
\usepackage[parfill]{parskip}
\tolerance=1000
\hyphenpenalty=1000
\hbadness=10000

\usepackage{bytefield}
\usepackage[bookmarks]{hyperref}
\usepackage{booktabs}
\usepackage{enumitem}

\usepackage{cochineal}
\usepackage[varqu,varl,var0]{zi4} % Inconsolata
\usepackage[scale=.95,type1]{cabin}
\usepackage[cochineal,bigdelims,cmintegrals,vvarbb]{newtxmath}
\usepackage[cal=boondoxo]{mathalfa}

\newcommand{\TITLE}{Kech Bank Protocol}
\newcommand{\SUBTITLE}{Versie 8}
\newcommand{\AUTHOR}{Bastiaan Teeuwen}
\newcommand{\INSTITUTE}{Hogeschool Rotterdam}
\newcommand{\DATE}{\today}

\title{\TITLE{} --- \SUBTITLE}
\author{\AUTHOR}
\date{\DATE}

\begin{document}

\begin{titlepage}

\raggedleft%

\rule{1pt}{\textheight}
\hspace{0.05 \textwidth}
\parbox[b]{0.75 \textwidth}{%
	{\huge \textbf{\TITLE}}\\[\baselineskip]
	{\Large \textbf{\SUBTITLE}}\\[3\baselineskip]
	{\large \AUTHOR}

	\vspace{0.5 \textheight}

	{\large \INSTITUTE}\\[\baselineskip]
	{\textit{\DATE}}\\[\baselineskip]
}

\end{titlepage}

\section{Introduction}
Dit document laat zien hoe het Kech Bank Protocol (KBP) geïmplementeerd moet
worden voor beide servers en clients.

Dit document verwijst meerdere malen naar constanten, structures en macros
gedefinieerd in \texttt{kbp.h}. Deze header zou samen met dit document geleverd
moeten zijn. De header is bestemd om in een C of C++ programma ge-include te
worden.

\section{Verbinding}
Verbinding met een server die KBP implementeert gebeurt over SSL/TLS. Het
gebruik van een server CA, server certificaat, client CA en client certificaat
zijn allemaal verplicht. Hierdoor kunnen ongeregistreerde ATM's niet zomaar
verbinding maken met de server en vice versa is het niet mogelijk een server te
spoofen.

De poort die gebruikt wordt is bij voorkeur TCP poort 8420 (\texttt{KBP\_PORT}).


\section{Request header}
Een request van de client naar de server ziet er als volgt uit (\texttt{struct
kbp\_request}):

\begin{center}
\begin{bytefield}{32}
	\bitheader{0, 7, 8, 15, 16, 31} \\
	\begin{leftwordgroup}{Header}
		\wordbox{1}{Magic 00:31} \\
		\bitbox{8}{Version 32:39} & \bitbox{8}{Type 40:47} &
		\bitbox{16}{Length 48:63} \\
		\bitbox{16}{Length 64:79} & \bitbox[lrt]{16}{}
	\end{leftwordgroup} \\
	\wordbox[lrb]{3}{Data \ldots}
\end{bytefield}
\end{center}

\begin{itemize}
	\item \textbf{\texttt{Magic}} is een constante en is altijd
		\texttt{KBP\_MAGIC}.
	\item \textbf{\texttt{Version}} is een integer om de versie van KBP aan
		te duiden en miscommunicaties tussen client en server te
		voorkomen. Dit is een constante (\texttt{KBP\_VERSION}).
	\item \textbf{\texttt{Type}} is het type request. Zie onderstaande
		secties voor meer informatie.
	\item \textbf{\texttt{Length}} is de lengte van \texttt{Data} in bytes.
		Dit mag nooit meer zijn dan \texttt{KBP\_LENGTH\_MAX}.
	\item \textbf{\texttt{Data}} (optioneel) bevat eventueel de data vereist
		om de request uit te voeren. De lengte van is afhankelijk van
		het \texttt{Type} request. Daarom is het slim om eerst de header
		binnen te halen en \texttt{Length} uit te lezen.
\end{itemize}

Indien \texttt{Magic} niet correct is, zal de request worden genegeerd door de
server.

Indien \texttt{Magic} correct is maar \texttt{Version} niet, zal de server de
verbinding direct verbreken.

Indien \texttt{Type} geen van de types in \texttt{kbp\_request\_t} is, zal de
request worden genegeerd door de server.

Indien \texttt{Length} langer is dan \texttt{KBP\_LENGTH\_MAX}, zal de request
worden genegeerd door de server.

Indien de server meer dan \texttt{KBP\_ERROR\_MAX} foutieve requests ontvangt
zal de verbinding worden verbroken.


\section{Reply header}
Een reply van de server naar de client heeft de volgende structuur
(\texttt{struct kbp\_reply}):

\begin{center}
\begin{bytefield}{32}
	\bitheader{0, 15, 16, 31} \\
	\begin{leftwordgroup}{Header}
		\wordbox{1}{Magic 00:31} \\
		\bitbox{8}{Version 32:39} & \bitbox{8}{Status 40:47} &
		\bitbox{16}{Length 48:63} \\
		\bitbox{16}{Length 64:79} & \bitbox[lrt]{16}{}
	\end{leftwordgroup} \\
	\wordbox[lrb]{3}{Data \ldots}
\end{bytefield}
\end{center}

\begin{itemize}
	\item \textbf{\texttt{Magic}} is een constante en is altijd
		\texttt{KBP\_MAGIC}.
	\item \textbf{\texttt{Version}} is een integer om de versie van KBP aan
		te duiden en miscommunicaties tussen client en server te
		voorkomen. Dit is een constante (\texttt{KBP\_VERSION}).
	\item \textbf{\texttt{Status}} geeft aan of de request met success is
		verwerkt. Dit verschilt per \texttt{Type} en staat in de
		volgende sectie beschreven per request.
	\item \textbf{\texttt{Length}} is de lengte van \texttt{Data} in bytes.
		Dit mag nooit meer zijn dan \texttt{KBP\_LENGTH\_MAX}.
	\item \textbf{\texttt{Data}} (optioneel) bevat eventueel de reply data.
		De lengte van is afhankelijk van het \texttt{Type} request.
		Daarom is het slim om eerst de header binnen te halen en
		\texttt{Length} uit te lezen.
\end{itemize}


\section{Request types}

\subsection{KBP\_T\_ACCOUNTS}
Dit type request vraagt een array van rekeningen op die horen bij de gebruiker
geassocieerd met de actieve sessie.

Deze request vereist een actieve sessie, \texttt{KBP\_S\_INVALID} zal anders
worden teruggestuurd.

Deze request heeft geen request data.

Deze request heeft de volgende reply data (\texttt{struct
kbp\_reply\_account[n]}):

\begin{center}
\begin{bytefield}{32}
	\bitheader{0, 23, 24, 31} \\
	\wordbox[lrt]{1}{IBAN 00:279} \\
	\skippedwords \\
	\wordbox[lr]{1}{} \\
	\bitbox[lrb]{24}{} & \bitbox{8}{Type 280:287} \\
	\wordbox{2}{Balance 288:351}
\end{bytefield}
\end{center}

\begin{itemize}
	\item \textbf{\texttt{IBAN}} is de IBAN van de rekening. Dit veld is
		altijd \texttt{KBP\_IBAN\_MAX} + 1 lang en moet worden
		afgesloten met een null terminator. Alleen ASCII tekens zijn
		toegestaan.
	\item \textbf{\texttt{Type}} is het type rekening. De rekening types
		staan in de \texttt{kbp\_account\_t} enumerator.
	\item \textbf{\texttt{Balance}} is het huidige saldo van de rekening in
		$\text{EUR} \cdot 100$ ($\text{EUR}\ 12.50$ wordt bijvoorbeeld
		$1250$).
\end{itemize}

Reply data zal niet worden verstuurd indien \texttt{Status} niet
\texttt{KBP\_S\_OK} is.

Het aantal rekeningen in de array kan bepaald worden door de \texttt{Length} uit
de reply header te delen door de lengte van bovenstaand structure.

Indien de request met success is verwerkt, zal \texttt{KBP\_S\_OK} worden
teruggestuurd. Indien er een interne fout optreed, zal \texttt{KBP\_S\_FAIL} de
\texttt{Status} zijn.

\subsection{KBP\_T\_PIN\_UPDATE}
Dit type request is een aanvraag van de client aan de server om de PIN code van
de kaart waarmee de sessie is gestart te veranderen.

Deze request vereist een actieve sessie, \texttt{KBP\_S\_INVALID} zal anders
worden teruggestuurd.

Deze request heeft de volgende request data nodig:

\begin{center}
\begin{bytefield}{32}
	\bitheader{0, 31} \\
	\wordbox[lrt]{1}{PIN 00:103} \\
	\skippedwords \\
	\wordbox[lrb]{1}{}
\end{bytefield}
\end{center}

\begin{itemize}
	\item \textbf{\texttt{PIN}} is de nieuwe PIN code. Een PIN kan alleen
		bestaan uit numerieke karakters en is minimaal
		\texttt{KBP\_PIN\_MIN} en maximaal \texttt{KBP\_PIN\_MAX} in
		lengte. De string moet worden afgesloten met een null
		terminator. Alleen ASCII tekens zijn toegestaan.
\end{itemize}

Deze request heeft geen reply data.

Indien de request met success is verwerkt, zal \texttt{KBP\_S\_OK} worden
teruggestuurd. Indien er een interne fout optreed, zal \texttt{KBP\_S\_FAIL} de
\texttt{Status} zijn.

\subsection{KBP\_T\_LOGIN}
Deze request start een nieuwe sessie.

Voor meer informatie over sessies, zie sectie~\ref{sec:sessies}.

Deze request heeft de volgende request data nodig:

\begin{center}
\begin{bytefield}{32}
	\bitheader{0, 23, 24, 31} \\
	\wordbox[lrt]{1}{Card UID 00:47} \\
	\bitbox[lrb]{16}{} & \bitbox[lrt]{16}{} \\
	\wordbox[lr]{1}{PIN 48:151} \\
	\skippedwords \\
	\wordbox[lrb]{1}{}
\end{bytefield}
\end{center}

\begin{itemize}
	\item \textbf{\texttt{Card UID}} De UID van de RFID kaart. De UID
		verwijst intern naar een \texttt{User ID} en \texttt{Card ID}.
		\texttt{De Card ID} wordt afzonderlijk per gebruiker (User ID)
		bijgehouden.
	\item \textbf{\texttt{PIN}} is de bijbehorende PIN code. Een PIN kan
		alleen bestaan uit numerieke karakters en is minimaal
		\texttt{KBP\_PIN\_MIN} en maximaal \texttt{KBP\_PIN\_MAX} in
		lengte. De string moet worden afgesloten met een null
		terminator. Alleen ASCII tekens zijn toegestaan.
\end{itemize}

Deze request heeft de volgende reply data:

\begin{center}
	\begin{bytefield}{8}
	\bitheader{0, 7} \\
	\wordbox{1}{kbp\_login\_res \tiny 00:07}
\end{bytefield}
\end{center}

De waarde van \texttt{kbp\_login\_res} kan een van de volgende constanten zijn:
\begin{itemize}
	\item \textbf{\texttt{KBP\_L\_GRANTED}}: De login gegevens kloppen en de
		sessie is vanaf nu actief.
	\item \textbf{\texttt{KBP\_L\_DENIED}}: De \texttt{PIN} komt niet
		overeen met de hash opgeslagen voor de combinatie van
		\texttt{User ID} en \texttt{Card ID} of een van de ID's komt
		niet voor in de database.
	\item \textbf{\texttt{KBP\_L\_BLOCKED}}: De \texttt{Card ID} van
		\texttt{User ID} is geblokkeerd omdat \texttt{KBP\_PINTRY\_MAX}
		overschreden is.
\end{itemize}

Reply data zal niet worden verstuurd indien \texttt{Status} niet
\texttt{KBP\_S\_OK} is.

Een counter voor \texttt{KBP\_PINTRY\_MAX} zal door de server intern worden
bijgehouden TODO

Indien de request met success is verwerkt, zal \texttt{KBP\_S\_OK} worden
teruggestuurd. Indien er een interne fout optreed, zal \texttt{KBP\_S\_FAIL} de
\texttt{Status} zijn.

\subsection{KBP\_T\_LOGOUT}
Deze request beëindigd de huidige sessie.

Deze request vereist een actieve sessie, \texttt{KBP\_S\_INVALID} zal anders
worden teruggestuurd.

Deze request heeft geen request of reply data.

Indien de request met success is verwerkt, zal \texttt{KBP\_S\_TIMEOUT} worden
teruggestuurd. Deze request kan niet falen, tenzij er geen actieve sessie is.

\subsection{KBP\_T\_TRANSACTIONS}
Deze request haalt een array van transacties op voor de gevraagd IBAN.

Deze request vereist een actieve sessie, \texttt{KBP\_S\_INVALID} zal anders
worden teruggestuurd.

Waarschuwing: deze request is nog niet geïmplementeerd in de referentie server
implementatie en kan mogelijk veranderen in de toekomst.

TODO

\subsection{KBP\_T\_TRANSFER}
Deze request voert een transactie tussen 2 rekeningen, schijft een bedrag van de
rekening af om op te nemen of stort geld op de rekening.

Deze request vereist een actieve sessie, \texttt{KBP\_S\_INVALID} zal anders
worden teruggestuurd.

Deze request heeft de volgende request data nodig:

\begin{center}
\begin{bytefield}{32}
	\bitheader{0, 23, 24, 31} \\
	\wordbox[lrt]{1}{Source IBAN 00:279} \\
	\skippedwords \\
	\wordbox[lr]{1}{} \\
	\bitbox[lrb]{24}{} & \bitbox[lrt]{8}{} \\
	\wordbox[lr]{1}{Destination IBAN 280:559} \\
	\skippedwords \\
	\wordbox[lr]{1}{} \\
	\bitbox[lrb]{16}{} & \bitbox[lrt]{16}{} \\
	\wordbox[lr]{1}{Amount 560:623} \\
	\bitbox[lrb]{16}{} & \bitbox{16}{\rule{\width}{\height}}
\end{bytefield}
\end{center}

\begin{itemize}
	\item \textbf{\texttt{Source IBAN}} is de IBAN van de bron rekening.
		Deze IBAN moet aan en rekening toegankelijk door de gebruiker
		geassocieerd met de actieve sessie gebonden zijn. Dit veld is
		altijd \texttt{KBP\_IBAN\_MAX} + 1 lang en moet worden
		afgesloten met een null terminator. Alleen ASCII tekens zijn
		toegestaan.
	\item \textbf{\texttt{Destination IBAN}} is de IBAN van de bestemming
		rekening. Dit veld is altijd \texttt{KBP\_IBAN\_MAX} + 1 lang en
		moet worden afgesloten met een null terminator. Alleen ASCII
		tekens zijn toegestaan.
	\item \textbf{\texttt{Amount}} is het aantal in $\text{EUR} \cdot 100$
		($\text{EUR}\ 12.50$ wordt bijvoorbeeld $1250$).
\end{itemize}

Indien \texttt{Source IBAN} leeg is (alleen een null terminator bevat), zal een
storing plaatsvinden. Dit betekent wel dat \texttt{Destination IBAN} naar een
rekening moet wijzen die toegankelijk is door de gebruiker geassocieerd met de
actieve sessie.

Indien \texttt{Destination IBAN} leeg is (alleen een null terminator bevat), zal
een geldopname plaatsvinden. Het is niet mogelijk geld op te nemen van de
rekening van een andere gebruiker \texttt{Source IBAN} moet daarom wel verwijzen
naar een rekening toegankelijk door de gebruiker geassocieerd met de actieve
sessie.

Indien \texttt{Amount} minder of gelijk is aan 0, zal \texttt{KBP\_S\_FAIL}
worden teruggestuurd.

Indien \texttt{Source IBAN} niet leeg is en gekoppeld is aan een rekening maar
het huidige saldo ontoereikend is, zal de transactie mislukken en wordt
\texttt{KBP\_S\_FAIL} teruggestuurd.

Deze request heeft geen reply data.

Indien de request met success is verwerkt, zal \texttt{KBP\_S\_OK} worden
teruggestuurd. Indien er een interne fout optreed, zal \texttt{KBP\_S\_FAIL} de
\texttt{Status} zijn.


\section{Sessies}
\label{sec:sessies}
\texttt{KBP\_T\_LOGIN} start een sessie. Zonder een sessie te starten is dit de
enige request die kan worden uitgevoerd. Indien er wel een andere request wordt
gestuurd zal de server meteen met \texttt{KBP\_S\_TIMEOUT} antwoorden.

Een sessie wordt alleen server side bijgehouden. Dit gebeurt in de referentie
server implementatie aan de hand van een token structure bevattende de
\texttt{User ID}, \texttt{Card ID} en de tijd van de start van de sessie +
\texttt{KBP\_TIMEOUT}.

Het sluiten van de connectie met de server betekent uiteraard dat het token, net
als bij het aanvragen van een logout (\texttt{KBP\_T\_LOGOUT}), vervalt.

\subsection{Sessie timeout}
Ten alle tijden kan een request worden beantwoord met \texttt{KBP\_S\_TIMEOUT}
als status. Dit betekent dat de sessie verlopen is en de gebruiker opnieuw zal
moeten inloggen. De timeout van een sessie is \texttt{KBP\_TIMEOUT} seconden.
Hoewel dit constante is, kan de waarde eventueel worden aangepast en clients
moeten er dus niet van uitgaan dat de timeout altijd \texttt{KBP\_TIMEOUT}
seconden is.

\end{document}
